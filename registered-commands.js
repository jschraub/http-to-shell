// Two types of commands:
// --- milight - uses built-in milight library to directly control milights
// --- shell - uses the OS shell to run custom commands and scripts

// Milight Commands
// Requires the following properties:
// --- ip (ip address of the milight controller)
// --- zone (zone of the milights to control)
// --- bulbType (rgb, rgbw, or white)
// Optional properties
// --- off (defaults to false - if ommitted, will turn the lights on)
// --- color (an hue value [0-255], or 'white', note quotes around white but no quotes for integers)
// --- brightness (percent value [0-100])

// Shell Commands
// Use the shell property to issue the exact shell command to use on the system

// Examples/Defaults below:

const Commands = [
  { url: '/milight/1/on/white', type: 'milight', ip: '192.168.1.200', zone: 1, bulbType: 'rgbw', color: 'white', brightness: 100 },
  { url: '/milight/1/on/blue', type: 'milight', ip: '192.168.1.200', zone: 1, bulbType: 'rgbw', color: 255, brightness: 50 },
  { url: '/milight/1/off', type: 'milight', ip: '192.168.1.200', zone: 1, bulbType: 'rgbw', off: true },
  { url: '/dishwasher/on', type: 'shell', shell: 'dishwasher on' },
  { url: '/dishwasher/off', type: 'shell', shell: 'dishwasher off' }
];

module.exports = Commands;
