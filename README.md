# HTTP-to-Shell #
## Version: 1.0 ##

### What is this repository for? ###

* A simple server designed to receive any HTTP request and run a corresponding shell command.

### How do I get set up? ###

* Simply pull down the code and run `node server.js`
* Review the `registered-commands.js` code to add or remove url paths and shell commands

### Contribution guidelines ###

* Create a Pull Request
* Give it a few days to be reviewed
* Check back for comments/change requests

### Who do I talk to? ###

* Jared Schraub (jaredschraub@outlook.com)
* Brian Freund