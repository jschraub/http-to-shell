const http = require('http');
const { exec } = require('child_process');

var { MilightController, commands } = require('node-milight-promise');

const RegisteredCommands = require('./registered-commands');

const DEFAULT_PORT = 9512;
const pIndex = process.argv.indexOf('-p');
const port = pIndex !== -1 ? parseInt(process.argv[pIndex + 1]) || DEFAULT_PORT : DEFAULT_PORT;

const CallCommand = cmd => {
  if (!cmd) {
    return;
  }
  exec(cmd, (error, stdout, stderror) => {
    // output if wanted for logging
  });
};

const SendResponse = res => code => {
  res.writeHead(code, {
    'Content-Type': 'text/plain'
  });
  res.end();
};

const OkResponse = res => SendResponse(res)(200);
const NotFoundResponse = res => SendResponse(res)(404);

http.createServer((req, res) => {
  console.log('Request Received');
  console.log('Request Path: ' + req.url);

  const command = RegisteredCommands.find(cmd => cmd.url === req.url);

  // check if the command was found
  if (!command) {
    console.log('Command not registered');
    NotFoundResponse(res);
    return;
  }

  // command was found
  if (command.type === 'milight') {
    console.log('Registered milight command found');
    const light = new MilightController({
      ip: command.ip,
      delayBetweenCommands: 100,
      commandRepeat: 2,
      type: 'all' // use all for better cross-support
    });

    const colorCommands = commands[command.bulbType];

    const commandList = [];
    if (command.off) {
      commandList.push(colorCommands.off(command.zone));
    } else {
      commandList.push(colorCommands.on(command.zone));
      if (command.color !== undefined) {
        if (typeof command.color === 'number') {
          console.log(`Found color: ${command.color}`);
          commandList.push(colorCommands.hue(command.color));
        } else if (typeof command.color === 'string' && command.color.toLowerCase() === 'white') {
          commandList.push(colorCommands.whiteMode(command.zone));
        }
      }
      if (command.brightness) {
        commandList.push(colorCommands.brightness(command.brightness));
      }
    }
    light.sendCommands(...commandList);
  
    OkResponse(res);
    return;
  }

  if (command.type === 'shell') {
    console.log('Registered shell command found');
    CallCommand(command.shell);
    OkResponse(res);
    return;
  }
}).listen(port);

console.log('Started on port: ' + port);
